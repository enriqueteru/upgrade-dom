//1.1 Usa querySelector para mostrar por consola el botón con la clase .**showme**
let showMe = document.querySelector('.showme');
//1.2 Usa querySelector para mostrar por consola el h1 con el id #pillado
let showMe2 = document.querySelector('#pillado');

//1.3 Usa querySelector para mostrar por consola todos los p
let p = document.querySelectorAll('p');

//1.4 Usa querySelector para mostrar por consola todos los elementos con la clase.pokemon
let pokemon = document.querySelectorAll('.pokemon');

//1.5 Usa querySelector para mostrar por consola todos los elementos con el atributo 
let testMe3 = document.querySelectorAll('[data-function="testMe"]')

//1.6 Usa querySelector para mostrar por consola el 3 personaje con el atributo 
let testMe4 = testMe3[2].textContent
console.log(testMe4);

//Bonus recorrer
for(item of testMe3){
    if(item.textContent === "Rick") {
        console.log(item.textContent)
}};






