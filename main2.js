
// Modificando el DOM---------------------------------------------------------------------------------------------------


// 2.1 Inserta dinamicamente en un html un div vacio con javascript.
let div1 = document.createElement('div');
let body = document.querySelector("body");
let referenceDiv = document.querySelector('.reference');
body.appendChild(div1);

//2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.

//2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.

//2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.

for(i = 1; i <= 6; i++){
let pLoop = document.createElement('p');
pLoop.innerHTML = `Soy Dinámico nº ${i}`
pLoop.className = 'nuevo-p';
referenceDiv.appendChild(pLoop)
}

//2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.

let fnInsertHere = document.querySelector('.fn-insert-here');
fnInsertHere.textContent = "Wubba Luppa dub dub";

/* 2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array. */
const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];

const listSocial = document.querySelector('#list');
let ul = document.createElement('ul');
listSocial.appendChild(ul);

for(item of apps){
let li = document.createElement('li')
li.textContent = item;
ul.appendChild(li);
};

//2.7 Elimina todos los nodos que tengan la clase .fn-remove-me
/*

1.0 forEach() loops through the NodeList and executes the specified action for each element

1.2 e => e.remove() removes the element from the DOM*/ 
let allFnRemove = document.querySelectorAll('.fn-remove-me').forEach(e => e.remove());


//2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. 
let enMedio = document.querySelector('#enmedio')
let newP = document.createElement('p');
newP.innerHTML = "<h1><b> ¡Voy en medio! </b></h1>";
body.insertBefore(newP, referenceDiv);


//2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here

let insertHere = document.querySelectorAll('.fn-insert-here');
for(item of insertHere){
item.innerHTML = '<p> ¡Voy dentro!</p>'
}